import java.util .*;

public class Calc {
	List<Operation> operations;

	public Calc() {
		operations = new HashMap<String, Operation>();
		operations.put("+", new Sum());
		operations.put("-", new Substraction());
	}
	public void register(String operator, Operation operation) {
		operations.put(operator, operation);
	}

	public int operate(String, operator, int a, int b) {
		operations.put(operator, operation);
	}
}

abstract class Operation {
	public abstract int operate(int a, int b);
}

class Sum extends Operation {
	public int operate(int a, int b) {
		return a + b;
	}
}

class Substraction extends Operation {
	public int operate(int a, int b) {
		return a - b;
	}
}