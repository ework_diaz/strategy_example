public class Client {
	public static void main(String[] args) {
//		Clase principal
		Calc calc = new Calc();
		
//		Adicionar nueva funcion
		calc.operations.put("@", new Arroba());
		
//		Operaciones
		int result = calc.operate("+", 2, 5);
		System.out.println(result);
		
		result = calc.operate("-", 5, 3);
		System.out.println(result);
		
		result = calc.operate("@", 8, 2);
		System.out.println(result);
	}
}

class Arroba extends Operation {
	public int operate(int a, int b) {
		return b * (a - 2) / (a + b);
	}
}